from airflow import DAG
from airflow.operators.python import PythonOperator
from airflow.utils.dates import days_ago
import pandas as pd
import os

dag = DAG("first_dag", start_date=days_ago(0, 0, 0, 0, 0))

def download_titanic_dataset():
    df = pd.read_csv("https://web.stanford.edu/class/archive/cs/cs109/cs109.1166/stuff/titanic.csv")
    df.to_csv("/opt/airflow/df.csv")

download_dataframe = PythonOperator(
    task_id="download_titanic_dataset",
    python_callable=download_titanic_dataset,
    dag=dag
)

def transform_titanic_dataset():
    df = pd.read_csv("/opt/airflow/df.csv")
    del df['Unnamed: 0']
    gr = df.groupby("Pclass").agg({"Survived": "mean"})
    gr.to_csv("/opt/airflow/output.csv")

transform_dataframe = PythonOperator(
    task_id="transform_dataset",
    python_callable=transform_titanic_dataset,
    dag=dag
)

os.chdir("/opt/airflow")

download_dataframe >> transform_dataframe