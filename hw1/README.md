# Домашняя работа №1. Airflow

## Описание
Проект представляет из себя набор файлов для разворачивания Apache Airflow с применением Docker. В проекте используется DAG (dag.py), который скачивает датасет и преобразует его, а затем сохраняет его в "/opt/airflow/output.csv".

## Запуск докера

```
git clone https://gitlab.com/aat71/devops.git
cd devops/hw1
docker-compose up -d
```


## Вход в Airflow
Попасть на Airflow через браузер можно по адресу
```
http://localhost:8080/
```