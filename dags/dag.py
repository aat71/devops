from airflow import DAG
from airflow.utils.dates import days_ago
from airflow.contrib.operators.spark_submit_operator import SparkSubmitOperator
from datetime import timedelta

dag = DAG(
    "second_dag",
    description="DAG for hw2",
    schedule_interval=timedelta(days=1),
    start_date=days_ago(0, 0, 0, 0, 0),
)

spark_job = SparkSubmitOperator(
    task_id='my_spark_job',
    application=f'/opt/airflow/spark/my_srcipt.py',
    name='my_spark_job',
    conn_id='spark_local',
    dag=dag)

spark_job
